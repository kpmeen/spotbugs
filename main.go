/*
main implements a Groovy, Graddle, Maven, Ant SAST analyzer, for use alone in your CI jobs or inside the GitLab SAST project
available at https://gitlab.com/gitlab-org/security-products/sast
*/

package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/plugin"
)

func init() {
	log.SetFormatter(&logutil.Formatter{Project: "spotbugs"})
}

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Find Sec Bugs analyzer for GitLab SAST"
	app.Author = "GitLab"
	app.Version = "10.8.0"
	app.Email = "gl-security-products@gitlab.com"

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		AnalyzeAll:   true,
		Convert:      convert.Convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
